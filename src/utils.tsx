export const tranformToFormUrlEncodedData = (formData: FormData) => {
    const dataKeys = Array.from(formData.keys());
    const formUrlEncodedRequestData = dataKeys
      .reduce((transformedRequestData: Array<string>, key, index) => {
        transformedRequestData = [
          ...transformedRequestData,
          encodeURIComponent(key),
          "=",
          encodeURIComponent(formData.get(key)?.toString() as string),
        ];
        if (index < dataKeys.length - 1) {
          transformedRequestData = [...transformedRequestData, "&"];
        }
        return transformedRequestData;
      }, [])
      .join("");
    return formUrlEncodedRequestData;
  };
