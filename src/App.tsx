import { FunctionComponent, useCallback, useEffect, useState } from "react";
import { Container, Stack } from "@mui/material";
import { fearless, Theme } from "@zip/theme";
import { Repayments, Dialogs, ThemeProvider, Buttons } from "@zip/components";
import { tranformToFormUrlEncodedData } from "./utils";
import type { Product } from "@zip/components/dist/modules/repayments/vgsCollect/VGSCollect.types";
// import * as address from "address";

const App: FunctionComponent<unknown> = () => {
  const theme: Theme = fearless;
  const [popupIsOpen, setPopupIsOpen] = useState(false);
  const [response, setResponse] = useState("");
  const [bearerToken, setBearerToken] = useState("");

  const buildBearerTokenPayload = useCallback(() => {
    const formData = new FormData();
    formData.set("client_id", `${process.env.REACT_APP_ZIP_TOKEN_CLIENT_ID}`);
    formData.set(
      "client_secret",
      `${process.env.REACT_APP_ZIP_TOKEN_CLIENT_SECRET}`
    );
    formData.set("grant_type", `${process.env.REACT_APP_ZIP_TOKEN_GRANT_TYPE}`);
    formData.set(
      "username",
      process.env.REACT_APP_VGS_COLLECT_PARAMS_ORIGINATOR_EMAIL as string
    );
    formData.set(
      "password",
      process.env.REACT_APP_VGS_COLLECT_PARAMS_PASSWORD as string
    );
    return formData;
  }, []);

  const fetchFreshBearerToken = useCallback(() => {
    fetch(`${process.env.REACT_APP_ZIP_TOKEN_GENERATION_URL}`, {
      method: "POST",
      headers: {
        Accept: "application/vnd.zipco.v3+json",
        "Content-Type": "application/x-www-form-urlencoded",
      },
      get body() {
        if (
          (this.headers as Record<string, unknown>)["Content-Type"] ===
          "application/x-www-form-urlencoded"
        ) {
          return tranformToFormUrlEncodedData(buildBearerTokenPayload());
        }
        return buildBearerTokenPayload();
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);
        setBearerToken(response.access_token);
      })
      .catch((error) => {
        console.error("Error in fetching Fresh Bearer Token");
        console.error("Error = ", error);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    fetchFreshBearerToken();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log("ip address = ", address.ip());
  return (
    <ThemeProvider theme={theme}>
      <Stack>
        <Container
          maxWidth="xs"
          sx={{
            border: `2px solid ${theme.colors.accent}`,
            borderRadius: `10px`,
            paddingBottom: "30px",
            // width: "500px",
            marginTop: "20px",
          }}
        >
          <Repayments.VGSCollect
            vaultId={
              process.env.REACT_APP_VGS_COLLECT_CONSTS_VAULT_ID as string
            }
            environment={process.env.REACT_APP_VGS_COLLECT_CONSTS_ENVIORNMENT}
            inboundPath={
              process.env.REACT_APP_VGS_COLLECT_PARAMS_PATH_INFO as string
            }
            initialIsDefault={true}
            doFormResetOnCancel={true}
            // product={Product.ZIP_PAY}
            product={
              Number(
                process.env.REACT_APP_VGS_COLLECT_PARAMS_PRODUCT_ID as string
              ) as Product
            }
            submitOptions={{
              method: "POST",
              headers: {
                Authorization: `Bearer ${bearerToken}`,
              },
              data: (formData: Record<string, unknown>) => {
                return {
                  data: {
                    type: "tokenization",
                    attributes: {
                      ...formData,
                      consumerId: Number(
                        process.env
                          .REACT_APP_VGS_COLLECT_PARAMS_CONSUMER_ID as string
                      ),
                      originatorEmail: process.env
                        .REACT_APP_VGS_COLLECT_PARAMS_ORIGINATOR_EMAIL as string,
                      isCheckout: false,
                    },
                  },
                };
              },
            }}
            cardNumberProperties={{
              name: "cardNumber",
              placeholder: "Card number*",
              validations: ["required", "validCardNumber"],
            }}
            cardHolderProperties={{
              name: "cardHolderName",
              placeholder: "Card holder name*",
              validations: [
                "required",
                `/^[a-zA-Z0-9 ]+$/`,
                // `/[a-zA-Z0-9 ]+/`,
              ],
            }}
            cardExpiryProperties={{
              name: "cardExpiry",
              placeholder: "Expiry* (MM/YYYY)",
              yearLength: 4,
              separator: "/",
              validations: ["required", "validCardExpirationDate"],
            }}
            cvvProperties={{
              name: "cardCvv",
              placeholder: "CVV*",
              maxLength: 3,
              validations: ["required", "validCardSecurityCode"],
            }}
            onSubmitSuccesstHandler={(status, successResponse) => {
              console.log("response = ", successResponse);
              setResponse(JSON.stringify(successResponse, null, 4));
              setPopupIsOpen(true);
            }}
            onSubmitErrortHandler={(errorResponse) => {
              console.log("response = ", errorResponse);
              setResponse(JSON.stringify(errorResponse, null, 4));
              setPopupIsOpen(true);
            }}
            onCancelHandler={(event) => console.log(event)}
          />
        </Container>
        <Container
          maxWidth="xs"
          sx={{
            padding: "10px",
            marginTop: "20px",
          }}
        >
          <Stack direction={"column"}>
            <Buttons.Secondary onClick={fetchFreshBearerToken}>
              Get Fresh Bearer Token
            </Buttons.Secondary>
          </Stack>
        </Container>
        <Dialogs.Basic
          maxWidth="md"
          fullWidth={true}
          // fullScreen={true}
          open={popupIsOpen}
          onClose={() => setPopupIsOpen(false)}
        >
          <pre>{response}</pre>
        </Dialogs.Basic>
        <Container
          maxWidth="md"
          style={{
            marginTop: "20px",
            wordWrap: "break-word",
            overflowX: "scroll",
          }}
        >
          <div style={{ textDecoration: "underline" }}>Response :</div>
          <pre>{response}</pre>
        </Container>
      </Stack>
    </ThemeProvider>
  );
};

export default App;
